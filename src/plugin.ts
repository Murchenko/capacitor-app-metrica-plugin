import { AppMetricaPlugin as IAppmetricaPlugin } from './definitions';
import { Plugins } from '@capacitor/core';

const AppmetricaPlugin = Plugins.AppMetrica as IAppmetricaPlugin;

export class AppMetrica {
  private appmetrica = AppmetricaPlugin;

  reportEvent (name: string, parameters?: Object) {
    return this.appmetrica.reportEvent({ name, parameters });
  }
}
