declare module '@capacitor/core' {
  interface PluginRegistry {
    AppMetrica: AppMetricaPlugin;
  }
}

export interface AppMetricaPlugin {
  reportEvent(options: { name: string; parameters?: Object }): Promise<void>;
}
