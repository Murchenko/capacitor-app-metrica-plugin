package com.sdaykat.capacitor.plugins.appmetrica;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.yandex.metrica.YandexMetrica;

import org.json.JSONObject;

@NativePlugin
public class AppMetrica extends Plugin {

    @PluginMethod
    public void reportEvent(PluginCall call) {
        String eventName = call.getString("name");
        JSONObject params = call.getObject("parameters");
        YandexMetrica.reportEvent(eventName, params.toString());

        JSObject ret = new JSObject();
        ret.put("success", true);
        call.success(ret);
    }
}
