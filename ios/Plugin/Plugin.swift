import Foundation
import Capacitor
import UserNotifications
import YandexMobileMetrica

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(AppMetrica)
public class AppMetrica: CAPPlugin {
    override public func load() {
        // Initializing the AppMetrica SDK.
        let apiKey = getConfigValue("apiKey") as! String
        let logs = getConfigValue("logs") as? Bool
        let configuration = YMMYandexMetricaConfiguration(apiKey: apiKey)
        configuration?.logs = logs ?? false

        YMMYandexMetrica.activate(with: configuration!)
    }


    @objc func reportEvent(_ call: CAPPluginCall) {
        guard let name = call.getString("name") else {
            return call.reject("Missing name argument")
        }
        let parameters = call.getObject("parameters") ?? nil

        YMMYandexMetrica.reportEvent(name, parameters: parameters, onFailure: { error in
            call.reject(error.localizedDescription)
        })

        call.success()
    }
}
